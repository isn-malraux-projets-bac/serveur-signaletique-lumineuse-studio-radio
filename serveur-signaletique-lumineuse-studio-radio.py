#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Créé le 30/03/2018

@authors : Killian/Abdessamad
"""
"""On import la bibliothèque qui nous permettra de gérer le serveur"""

import liblo #bibliothèque gestion serveur
from threading import Thread #bibliothèque permettant de faire des threads
import configparser #bibliothèque pour fichier de configuration
import RPi.GPIO as GPIO #bibliothèque permettant de communiquer avec support matériel
import time #permet de faire des pauses

INTERRUPTEUR = False #initialiation de l'état
CLEF_SECURITE = str() #déclaration de la variable et son type

class Serv(Thread): #début du serveur 
    def __init__(self, port=1234, proto=liblo.UDP):
        Thread.__init__(self) #initialisation du thread
        self.port = port  #initialisation du port
        self.protocole = proto #initialisation du protocole
        self.serveur = liblo.Server(self.port, self.protocole) # initialisation du serveur
        print("Thread initialisé ...") 
        print("Le serveur écoute sur le port '{0}' ".format(self.port))
        self.serveur.add_method("/signaletique/enregistrement/interrupteur", "sT", interrupteur_callback) #le serveur attend ce message
        self.serveur.add_method("/signaletique/enregistrement/interrupteur", "sF", interrupteur_callback) #le serveur attend ce message
        self.serveur.add_method(None, None, capture_tout_le_reste_callback)  # Doit toujours être la dernière, quand ce n'est pas un message valide

    def run(self):
    	while True:
            self.serveur.recv(50) #le serveur actualise toutes le 50 ms

def interrupteur_callback(chemin, parametres):
    global INTERRUPTEUR #importe la variable
    global CLEF_SECURITE #importe la variable
    cle_securite = parametres[0] #correspond au s de "sT"
    etat_demande = parametres[1] #correspond au T de "sT"
    print("Demande reçue pour mettre l'interrupteur sur '{}'".format(etat_demande)) 
    if cle_securite == CLEF_SECURITE : #vérifie que la clef est bonne
        print("Clé de sécurité '{}' vérifiée, demande acceptée".format(cle_securite))
        ancien_etat = INTERRUPTEUR 
        INTERRUPTEUR = etat_demande #met à jour l'état de la variale INTERRUPTEUR
        print("Interrupteur positionné depuis '{}' vers '{}'".format(ancien_etat, etat_demande))
    else: # la clef n'est pas valide
        print("Clé de sécurité '{}' invalide, demande refusée".format(cle_securite)) #montre la clef invalide
        print("Interrupteur conservé en l'état '{}'".format(INTERRUPTEUR))

def capture_tout_le_reste_callback(chemin, parametres, types, origine): #fonction lancée si demande non comprise
    print("Réception d'un message OSC provenant de '{}' et demandant '{}'".format(origine.url, chemin))
    for valeur_parametre, type in zip(parametres, types):
        print("Paramètre de type '{}' = {}".format(type, valeur_parametre))

LedPin = 40    # pin 40

def initialisation():
    print("Initialisation des ports GPIO de la RPi")
    GPIO.setmode(GPIO.BOARD)       # met le gpio avec son emplacement physiquement
    GPIO.setup(LedPin, GPIO.OUT)   # met le pin 40 en output
    GPIO.output(LedPin, GPIO.HIGH) # met pin 40 en hight (+3,3V)

def Allumer_les_Led():
    print("Allumage de la LED")
    GPIO.output(LedPin, GPIO.HIGH)  # led allumée

def Eteindre_les_Led():
    print("Extinction de la LED")
    GPIO.output(LedPin, GPIO.LOW)  # led éteinte

def Faire_Clignoter_les_Led():
    global INTERRUPTEUR
    while True:
        time.sleep(0.01)  # Pour éviter d'engorger l'occupation du CPU
        if INTERRUPTEUR == True: #vérification de l'état de l'interrupteur
            Allumer_les_Led()
            time.sleep(1) #ne passe pas à l'instruction suivante pendant 1s
            Eteindre_les_Led()
            time.sleep(1) #ne passe pas à l'instruction suivante pendant 1s

if __name__ == '__main__':     # début de la boucle
    config = configparser.ConfigParser()  # Instanciation d'un ConfigParser
    config.read("fichier_config.txt")  # Lecture du fichier contenant la configuration
    CLEF_SECURITE = config["DEFAULT"]["CLEF"]  # récupération de l'élément de configuration CLEF
    print("Votre clé d'accès est '{}' ".format(CLEF_SECURITE)) #message montrant la clef rentrée

    serveur_osc_paralelle = Serv()  # Instanciation d'un Serv
    serveur_osc_paralelle.start()  # Lancement de la paralellisation du serveur

    initialisation() 
    Faire_Clignoter_les_Led()